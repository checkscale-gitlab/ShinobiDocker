#!/bin/sh
# set -e

# Create a file like this to enable MQTT support for Shinobi
# You'll just have to uncomment the following lines.
# Read about the MQTT bridge for Shinobi on https://hub.shinobi.video/articles/view/xEMps3O4y4VEaYk

# cd /opt/shinobi
# mkdir -p /opt/shinobi/libs/customAutoLoad
# wget -q https://gitlab.com/geerd/shinobi-mqtt/raw/master/mqtt.js -O /opt/shinobi/libs/customAutoLoad/mqtt.js
# npm install mqtt
